.PHONY: migrate
migrate:
	sql-migrate up

.PHONY: migrate_down
migrate-down:
	sql-migrate down

lint:
	golangci-lint run

.PHONY: test
test:
	go test ./...