package configs

import (
	"log"
	"os"

	"github.com/fsnotify/fsnotify"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// holds parsed config params from YAML by Viper
var cfg Config

// Config represents yaml params
type Config struct {
	Log        LogConfig    `yaml:"log"`
	ServerName string       `yaml:"serverName"`
	Rabbit     RabbitConfig `yaml:"rabbit"`
	DB         DBConfig     `yaml:"db"`
}

// LogConfig represent yaml log params
type LogConfig struct {
	Pretty bool   `yaml:"pretty"`
	Level  string `yaml:"level"`
}

// RabbitConfig represent yaml rabbitmq params
type RabbitConfig struct {
	URI          string   `yaml:"uri"`
	ConsumerName string   `yaml:"consumer_name"`
	RoutingKeys  []string `yaml:"routing_keys"`
}

// DBConfig represent yaml db params
type DBConfig struct {
	MySQL MySQLConfig `yaml:"mysql"`
}

// MySQLConfig represent yaml mysql params
type MySQLConfig struct {
	URI       string `yaml:"uri"`
	TableName string `yaml:"table_name"`
}

const (
	// YamlExtension extension name
	YamlExtension = "yaml"
	debugLevel    = "debug"
)

// Load config from YAML into cfg
func Load(configPath string) error {
	viper.SetConfigName(YamlExtension)
	viper.SetConfigFile(configPath)
	if err := viper.ReadInConfig(); err != nil {
		return errors.Errorf("Can't read config file: %s", err)
	}

	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Printf("Config file %s changed. Exiting.", e.Name)
		os.Exit(1)
	})

	if err := viper.Unmarshal(&cfg, func(decoderCfg *mapstructure.DecoderConfig) {
		decoderCfg.TagName = YamlExtension
	}); err != nil {
		return errors.Wrapf(err, "failed to unmarshal config file %s into %T", configPath, cfg)
	}

	if cfg.Log.Level == "" {
		cfg.Log.Level = debugLevel
	}
	return nil
}

// Get cfg data
func Get() Config {
	return cfg
}
