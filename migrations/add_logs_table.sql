-- +migrate Up
CREATE TABLE logs (
    id                 INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    server             VARCHAR(50),
    robot_name         VARCHAR(50),
    code               VARCHAR(20),
    message            VARCHAR(128),
    raw_message        VARCHAR(255),

    dt TIMESTAMP DEFAULT now() NOT NULL
);
-- +migrate Down
DROP TABLE logs;