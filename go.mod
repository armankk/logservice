module logservice

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/golang/mock v1.6.0
	github.com/gorilla/mux v1.7.3
	github.com/mitchellh/mapstructure v1.4.2
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.26.0
	github.com/spf13/viper v1.9.0
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.7.0
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
	gorm.io/driver/mysql v1.1.3
	gorm.io/gorm v1.22.2
)
