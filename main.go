package main

import (
	"fmt"
	"io"
	"logservice/configs"
	"logservice/drivers/dbs/mysql"
	"logservice/drivers/queues/rabbitmq"
	"logservice/handlers"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"
)

const (
	ConfigPath = "config.yaml"

	IPInsideContainer = "0.0.0.0"
	HealthPort        = 9091
)

var version string

func main() {
	if err := run(); err != nil {
		log.Fatal().Stack().Err(err).Msg("Failed to start service")
	}
	os.Exit(0)
}

func run() error {
	if err := configs.Load(ConfigPath); err != nil {
		return err
	}
	cfg := configs.Get()

	// Logger
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	var writer io.Writer
	if cfg.Log.Pretty {
		writer = zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	}
	logLvl := zerolog.DebugLevel
	if lvl, err := zerolog.ParseLevel(cfg.Log.Level); err == nil {
		logLvl = lvl
	}
	log.Logger = zerolog.New(writer).
		With().
		Caller().
		Timestamp().
		Logger().
		Level(logLvl)

	// MySQL
	db, err := mysql.Config(cfg.DB.MySQL).Open()
	if err != nil {
		return err
	}

	defer mysql.Close(db)

	// RabbitMQ
	rabbit, err := rabbitmq.Config(cfg.Rabbit).Open()
	if err != nil {
		return err
	}
	defer rabbit.Shutdown()

	// Health server
	httpErrs := make(chan error)

	healther := handlers.NewHealther(version, db, rabbit)
	go checkHealth(httpErrs, healther)

	// Handle log writer
	handlers.NewLogWriter(db, rabbit, cfg.Rabbit.RoutingKeys, cfg.DB.MySQL.TableName).Handle()

	signals := make(chan os.Signal, 1)
	signal.Notify(signals,
		syscall.SIGHUP,
		syscall.SIGILL,
		syscall.SIGINT,
		syscall.SIGQUIT,
		syscall.SIGABRT,
		syscall.SIGTERM,
		syscall.SIGTRAP,
	)

	select {
	case err := <-httpErrs:
		return errors.Wrap(err, "http server error")
	case sig := <-signals:
		// Graceful shutdown
		log.Info().Msgf("Received signal: %s", sig)
		defer log.Info().Msg("Shutdown is complete")

		var shutdownErrMessages []string

		if len(shutdownErrMessages) > 0 {
			return errors.New(strings.Join(shutdownErrMessages, "; "))
		}
		return nil
	}
}

func checkHealth(errs chan error, healther *handlers.Healther) {
	log.Info().Msgf("Starting Health HTTP server on port %d...", HealthPort)
	err := http.ListenAndServe(fmt.Sprintf("%s:%d", IPInsideContainer, HealthPort), healther.Handle())
	if err != nil && !errors.Is(err, http.ErrServerClosed) {
		errs <- err
	}
}
