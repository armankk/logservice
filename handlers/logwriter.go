package handlers

import (
	"encoding/json"
	"logservice/drivers/queues/rabbitmq"
	"logservice/models"
	"time"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"

	"github.com/streadway/amqp"
)

// LogWriter controls Handle function
type LogWriter struct {
	db         *gorm.DB
	rabbitmq   *rabbitmq.Connection
	queueNames []string
	tableName  string
}

// NewLogWriter creates instance of LogWriter
func NewLogWriter(
	db *gorm.DB,
	rabbitmq *rabbitmq.Connection,
	qNames []string,
	tableName string,
) *LogWriter {
	return &LogWriter{
		db:         db,
		rabbitmq:   rabbitmq,
		queueNames: qNames,
		tableName:  tableName,
	}
}

// Data from queue performing
type Data struct {
	ch  <-chan amqp.Delivery
	err error
}

// Alarm represents incoming alarm queue message
type Alarm struct {
	Code           string    `json:"code"`
	Data           AlarmData `json:"data"`
	DebugCounter   int       `json:"debug_counter"`
	DebugTimestamp string    `json:"debug_timestamp"`
	Level          int       `json:"level"`
	Message        string    `json:"message"`
	ServerName     string    `json:"servername"`
	Source         string    `json:"source"`
	SourceName     string    `json:"sourcename"`
	Time           string    `json:"time"`
	RawMessage     string    `json:"-"`
}

// AlarmData of Alarm
type AlarmData struct {
	ExchangeAccount string `json:"exchange_account"`
	Instrument      string `json:"instrument"`
	RobotName       string `json:"robot_name"`
}

// Handle performs log creation
func (l *LogWriter) Handle() {
	data := make(chan Data)
	go l.Check(data)

	logQueries := models.NewLogQueries(l.db, l.tableName)

	d := <-data
	if d.err != nil {
		log.Error().Stack().Err(d.err).Msg("Rabbitmq consume error")
		return
	}
	for msg := range d.ch {
		var alarm Alarm
		alarm.RawMessage = string(msg.Body)

		err := json.Unmarshal(msg.Body, &alarm)
		if err != nil {
			log.Error().Stack().Err(err).Msg("Alarm unmarshal error")
		}
		err = logQueries.Create(alarm.Convert())
		if err != nil {
			log.Error().Stack().Err(err).Msg("Log create error")
		}
	}
}

// Convert Alarm to models.Log
func (a Alarm) Convert() models.Log {
	return models.Log{
		DateTime:   time.Now(),
		Server:     a.ServerName,
		RobotName:  a.Data.RobotName,
		Code:       a.Code,
		Message:    a.Message,
		RawMessage: a.RawMessage,
	}
}

// Check rabbitmq queues for messages
func (l *LogWriter) Check(d chan Data) {
	messages, err := l.rabbitmq.ExchangeSub(l.queueNames)
	d <- Data{ch: messages, err: err}
	time.Sleep(time.Millisecond * 50)
}
