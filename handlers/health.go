package handlers

import (
	"encoding/json"
	"logservice/drivers/queues/rabbitmq"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

// Healther controls Handle function
type Healther struct {
	version  string
	db       *gorm.DB
	rabbitmq *rabbitmq.Connection
}

// NewHealther creates instance of Healther
func NewHealther(version string, db *gorm.DB, rabbitmq *rabbitmq.Connection) *Healther {
	return &Healther{
		version:  version,
		db:       db,
		rabbitmq: rabbitmq,
	}
}

type HealthStatuses struct {
	Version string `json:"version"`
	Rabbit  string `json:"rabbitmq"`
	MySQL   string `json:"mysql"`
}

type ServeHTTP func(http.ResponseWriter, *http.Request)

// Handle health routes
func (h *Healther) Handle() http.Handler {
	httpHandler := mux.NewRouter()
	httpHandler.HandleFunc("/ready", h.ready)
	return httpHandler
}

func (h *Healther) ready(w http.ResponseWriter, _ *http.Request) {
	statuses := HealthStatuses{
		Version: h.version,
		Rabbit:  "ok",
		MySQL:   "ok",
	}

	sqlDb, err := h.db.DB()
	if err != nil {
		statuses.MySQL = err.Error()
	}
	if err := sqlDb.Ping(); err != nil {
		statuses.MySQL = err.Error()
	}

	if err := h.rabbitmq.Error(); err != nil {
		statuses.Rabbit = err.Error()
	}
	err = json.NewEncoder(w).Encode(statuses)
	if err != nil {
		log.Error().Stack().Err(err).Msg("Response encode error")
	}
}
