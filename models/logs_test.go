package models

import (
	"database/sql"
	"database/sql/driver"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Suite struct {
	suite.Suite
	DB         *gorm.DB
	mock       sqlmock.Sqlmock
	repository LogRepository
}

func (s *Suite) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(mysql.New(mysql.Config{
		Conn:                      db,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	require.NoError(s.T(), err)

	s.repository = NewLogQueries(s.DB, "los")
	s.DB.Debug()
}

func (s *Suite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInit(t *testing.T) {
	suite.Run(t, new(Suite))
}

func (s *Suite) TestCreate() {
	var (
		log = Log{
			DateTime:   time.Now(),
			Server:     "linux",
			RobotName:  "bnd-robot",
			Code:       "123",
			Message:    "Price check error",
			RawMessage: "...Price check error",
		}
	)

	s.mock.ExpectBegin()
	s.mock.ExpectExec("INSERT").
		WithArgs(log.DateTime, log.Server, log.RobotName, log.Code, log.Message, log.RawMessage).
		WillReturnResult(driver.ResultNoRows)
	s.mock.ExpectCommit()

	err := s.repository.Create(log)
	require.NoError(s.T(), err)
}
