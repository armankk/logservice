package models

import (
	"time"

	"gorm.io/gorm"
)

// Log model representation
type Log struct {
	ID         int       `gorm:"column:id"`
	DateTime   time.Time `gorm:"column:dt"`
	Server     string    `gorm:"column:server"`
	RobotName  string    `gorm:"column:robot_name"`
	Code       string    `gorm:"column:code"`
	Message    string    `gorm:"column:message"`
	RawMessage string    `gorm:"column:raw_message"`
}

// LogQueries controls Log entity
type LogQueries struct {
	db        *gorm.DB
	TableName string
}

// NewLogQueries creates instance of LogQueries
func NewLogQueries(db *gorm.DB, tableName string) *LogQueries {
	return &LogQueries{db: db, TableName: tableName}
}

// LogRepository functions interface
type LogRepository interface {
	Create(logData Log) error
}

// Create Log entry in db
func (l *LogQueries) Create(logData Log) error {
	return l.db.Table(l.TableName).Create(&logData).Error
}
