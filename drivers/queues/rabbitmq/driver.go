package rabbitmq

import (
	"encoding/json"
	"fmt"
	"logservice/configs"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
)

const (
	HealthCheckChannel = "healthcheck"
	sourceExchange     = "amq.topic"
)

// Config configs.RabbitConfig alias
type Config configs.RabbitConfig

// Connection controls rabbitmq entity
type Connection struct {
	conn         *amqp.Connection
	ch           *amqp.Channel
	consumerName string
	err          chan error
}

// Open rabbitmq connection
func (c Config) Open() (*Connection, error) {
	log.Printf("Rabbit: dialing %q", c.URI)
	conn, err := amqp.DialConfig(c.URI, amqp.Config{})
	if err != nil {
		return nil, fmt.Errorf("DialConfig: %w", err)
	}

	rabbitConn := &Connection{
		conn:         conn,
		consumerName: c.ConsumerName,
	}
	go rabbitConn.Check()

	return rabbitConn, nil
}

// Check if rabbitmq connection closed
func (c *Connection) Check() {
	e := <-c.conn.NotifyClose(make(chan *amqp.Error))
	if e != nil {
		c.err <- e
		log.Error().Stack().Err(e).Msgf("Rabbit disconnected with error: %s", e.Reason)
	}
}

// Error returns connection errors
func (c *Connection) Error() error {
	select {
	case err := <-c.err:
		return err
	default:
		return nil
	}
}

// Shutdown closes rabbitmq connection
func (c *Connection) Shutdown() {
	if c.conn == nil {
		return
	}

	log.Info().Msg("Closing connection with Rabbit...")
	if err := c.conn.Close(); err != nil {
		log.Error().Stack().Err(err).Msg("Rabbit connection close error")
		return
	}
	log.Info().Msg("Rabbit shutdown OK")
}

// ExchangeSub subscribe by several routing keys
func (c *Connection) ExchangeSub(queues []string) (<-chan amqp.Delivery, error) {
	ch, err := c.conn.Channel()
	if err != nil {
		return nil, err
	}
	c.ch = ch

	q, err := ch.QueueDeclare(
		c.consumerName, // name of the queue
		true,           // durable
		false,          // delete when unused
		false,          // exclusive
		false,          // noWait
		nil,            // arguments
	)
	if err != nil {
		return nil, fmt.Errorf("Queue Declare: %w", err)
	}

	log.Printf("declared Queue (key %q %d messages, %d consumers), binding to Exchange",
		q.Name, q.Messages, q.Consumers)

	// bind several keys
	for _, queue := range queues {
		if err = ch.QueueBind(
			q.Name,         // name of the queue
			queue,          // bindingKey
			sourceExchange, // sourceExchange
			false,          // noWait
			nil,            // arguments
		); err != nil {
			return nil, fmt.Errorf("Queue Bind: %w", err)
		}
	}

	return ch.Consume(
		q.Name,         // queue name
		c.consumerName, // consumer
		true,           // auto-ack
		false,          // exclusive
		false,          // no local
		false,          // no wait
		nil,            // arguments
	)
}

// Publish message into channel
func (c *Connection) Publish(msg interface{}, channel string) error {
	msgBody, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	envelope := amqp.Publishing{
		Headers:         amqp.Table{},
		ContentType:     "",
		ContentEncoding: "",
		DeliveryMode:    0,
		Priority:        0,
		CorrelationId:   "",
		ReplyTo:         "",
		Expiration:      "",
		MessageId:       "",
		Timestamp:       time.Now(),
		Type:            "",
		UserId:          "",
		AppId:           "",
		Body:            msgBody,
	}

	err = c.ch.Publish(sourceExchange, channel, false, false, envelope)
	if err != nil {
		return err
	}

	return nil
}
