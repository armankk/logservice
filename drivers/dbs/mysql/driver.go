package mysql

import (
	"database/sql"
	"logservice/configs"
	"time"

	"github.com/rs/zerolog/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const (
	DriverName = "mysql"
)

// Config configs.MySQLConfig alias
type Config configs.MySQLConfig

// Open db connection
func (c Config) Open() (*gorm.DB, error) {
	log.Printf("Mysql: dialing %q", c.URI)
	sqlDB, err := sql.Open(DriverName, c.URI)
	if err != nil {
		return nil, err
	}
	db, err := gorm.Open(mysql.New(mysql.Config{
		Conn: sqlDB,
	}), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	err = sqlDB.Ping()
	if err != nil {
		return nil, err
	}
	sqlDB.SetConnMaxLifetime(time.Minute * 2)
	sqlDB.SetMaxOpenConns(1)
	sqlDB.SetMaxIdleConns(1)
	return db, nil
}

// Close db connection manually
func Close(db *gorm.DB) {
	log.Info().Msg("Closing connection with MySQL...")
	sqlDB, err := db.DB()
	if err != nil {
		log.Error().Stack().Err(err).Msg("MySQL connection close error")
	}

	sqlDB.Close()
	log.Info().Msg("MySQL shutdown OK")
}
