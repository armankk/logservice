FROM golang:1.16-buster as BUILDER

WORKDIR /tmp/logservice

COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .

ARG VERSION
RUN CGO_ENABLED=0 go build -o logservice -ldflags "-X main.version=${VERSION}" *.go

FROM alpine
COPY --from=BUILDER /tmp/logservice/logservice /usr/local/bin

CMD ["/usr/local/bin/logservice"]
