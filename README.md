# Service for saving logs

Service listens to specified rabbitmq queues and writes logs to mysql.

## Usage

`config.yaml` contains all env variables and dsns

**Note!** if you change db dsn change also change it in `dbconfig.yml` file

- `make migrate` to migrate up db migrations 
- `make migrate-down` to migrate down db migrations